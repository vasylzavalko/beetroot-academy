<?php
$alert = 0;
$alertMesage = [
	'1' => 'Немає доступу до бази. Спробуйте пізніше.',
	'2' => 'Введіть логін та пароль.',
	'3' => 'Користувач із введеним логіном відсутній в базі.',
	'4' => 'Ви ввели недійсний пароль. Спробуйте ще раз',
	'5' => 'Ви успішно авторизувались!',
	'6' => 'Кількість авторизації:',
	'7' => 'У Вас залишилось дві успішних спроб щоб авторизуватись!',
	'8' => 'У Вас залишилась одна успішна спроба шоб авторизуватись!',
	'9' => 'Після неуспішної авторизації ваш логін буде заблоковано на 2 хв.',
	'10' => 'Ваш логін заблоковано на 2 хв.',
	'11' => 'Спробуйте авторизуватись через',
	'12' => 'секунд.',
];

$bannedArray = [0,0];
$bannedTime = 120;
$bannedSec = 0;
$banned = 0;

$users = [];
$foder = __DIR__.DIRECTORY_SEPARATOR."users";
$fileTxt = $foder.DIRECTORY_SEPARATOR.'users.txt';
$fileJson = $foder.DIRECTORY_SEPARATOR.'json.txt';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	if(isset($_POST['submit']) && $_POST['submit']=='submit_form' ){
		
		// Прочитати текстовий файл та згенерувати в масив $users
		if (file_exists($fileTxt)) {
			$lines = file($fileTxt);
			foreach($lines as $value){
				$valueArray = explode(" ",$value);
				if(count($valueArray)==2){
					$users[trim($valueArray[0])] = trim($valueArray[1]);
				}
			}
		}else{
			$alert = $alertMesage[1];
		}
		
		// Прочитати Json файл та згенерувати в масив $users
		/*
		if (file_exists($fileJson)) {
			$lines = file_get_contents($fileJson);
			$lines = json_decode($lines,TRUE);
			foreach($lines as $value){
				if( isset($value['login']) && isset($value['pass']) && !empty($value['login']) && !empty($value['pass']) ){
					$users[$value['login']] = $value['pass'];
				}
			}
		}else{
			$alert = $alertMesage[1];
		}
		*/
		
		
		if($alert==0){
		
			// Перевіряєм чи введені поля форми логін та пароль
			if( isset($_POST['login']) && !empty($_POST['login']) && isset($_POST['password']) && !empty($_POST['password']) ){
				// Перевіряєм чи є користувач із введеним логіном
				if( isset($users[$_POST['login']]) ){
					
					// Перевіряємо чи є файл користувача в папці banned і записуємо дані в масив $bannedArray
					$folderBanned = __DIR__.DIRECTORY_SEPARATOR."banned";
					$fileBannedUser = $folderBanned.DIRECTORY_SEPARATOR.$_POST['login'].".txt";
					if (!file_exists($folderBanned)) {
						mkdir($folderBanned, 0777, true);
					}
					if (file_exists($fileBannedUser)) {
						$bannedArray = file_get_contents($fileBannedUser);
						$bannedArray = explode(" ",$bannedArray);
						if(count($bannedArray)>1){
							$bannedArray = [trim($bannedArray[0]),trim($bannedArray[1])];
							if(!is_numeric($bannedArray[0])){
								$bannedArray[0] = 0;
							}
							if(!is_numeric($bannedArray[1])){
								$bannedArray[1] = 0;
							}
						}else{
							$bannedArray = [0,0];
							
						}
					}
					
					// Якщо є дата бану, то пеервіряємо чи не застаріла
					if( $bannedArray[1]+$bannedTime>time() ){
						$banned = 1;
						$bannedSec = $bannedArray[1]+$bannedTime-time();
					}					
					
					if ( $banned==0 ){
						
						// Якщо не забанений то продовжуємо
						if ( password_verify($_POST['password'],$users[$_POST['login']]) ) {

							// Записуємо файл статистики після успішної авторизації
							$folder = __DIR__.DIRECTORY_SEPARATOR."stat";
							$fileUser = $folder.DIRECTORY_SEPARATOR.$_POST['login'].".txt";
							if (!file_exists($foder)) {
								mkdir($foder, 0777, true);
							}
							
							if (!file_exists($fileUser)) {
								$count = 1;
							}else{
								$count = file_get_contents($fileUser);
								if(is_numeric($count)){
									$count++;
								}else{
									$count = 1;
								}
							}
							file_put_contents($fileUser, $count);
							
							$bannedArray = [0,0];
							
							$alert = $alertMesage[5]."<br>";
							$alert .= $alertMesage[6].$count;
							
						} else {
							
							// Пароль невірний
							$alert = $alertMesage[4];
							
							// Перевіряємо кількість невдалих авторизацій
							$bannedArray[0]++;
							switch($bannedArray[0]){
								case 1:
									$alert .= "<br>".$alertMesage[7];
								break;
								case 2:
									$alert .= "<br>".$alertMesage[8];
									$alert .= "<br><b>".$alertMesage[9]."</b>";
								break;
								case 3:
									$alert .= "<br><b>".$alertMesage[10]."</b>";
									$bannedArray[1] = time();
								break;
								default:
									$alert .= "<br>".$alertMesage[7];
									$bannedArray[0] = 1;
								break;
							}
							
						}
						
						$bannedArray = implode(" ",$bannedArray);
						file_put_contents($fileBannedUser, $bannedArray);
						
					}else{
						
						// Якщо забанений то виводимо повідомлення
						$alert = "<b>".$alertMesage[10]."</b><br>";
						$alert .= $alertMesage[11]." ".$bannedSec." ".$alertMesage[12];
						
					}
					
				}else{
					$alert = $alertMesage[3];
				}
			}else{
				$alert = $alertMesage[2];
			}
		}
	}
}

?>

<?php if( $alert>0 ){ ?>
<div style="padding:10px;border:2px solid #ccc;margin:10px 0px;">
<?php echo $alert; ?>
</div>
<?php } ?>

<h1>Авторизація</h1>
<form method="POST" action="/homework-11/">
	<label>Логін:</label><br>
	<input name="login" type="text"><br><br>
	<label>Пароль:</label><br>
	<input name="password" type="password"><br><br>
	<button type="submit" name="submit" value="submit_form">Увійти</button>
</form>