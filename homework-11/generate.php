<?php

$users[1] = [ 'login' => 'petro', 'pass' => '12345' ];
$users[2] = [ 'login' => 'oksana', 'pass' => '123456' ];
$users[3] = [ 'login' => 'vasyl', 'pass' => '1234' ];
$users[4] = [ 'login' => 'pavlo', 'pass' => '12345' ];
$users[5] = [ 'login' => 'ira', 'pass' => '1234567' ];

// Хешуємо паролі
foreach($users as &$value){
	$value['pass'] = password_hash($value['pass'],PASSWORD_DEFAULT);
}
unset($value);

// Файли з даними
$foder = __DIR__.DIRECTORY_SEPARATOR."users";
$fileTxt = $foder.DIRECTORY_SEPARATOR.'users.txt';
$fileJson = $foder.DIRECTORY_SEPARATOR.'json.txt';
if (!file_exists($foder)) {
    mkdir($foder, 0777, true);
}

// Генеруємо дату
$dataText = "";
foreach($users as $value){
	$dataText .= $value['login']." ".$value['pass']."\n";
}
$dataJson = json_encode($users);

// Записуємо в файли
file_put_contents($fileTxt, $dataText);
file_put_contents($fileJson, $dataJson);

?>