# Homework #15
![tadabase-structure.jpg](./tadabase-structure.jpg)

# Homework #16

### Table Category
```
CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
```
### Table Product
```
CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku` varchar(10) NOT NULL,
  `title` varchar(60) NOT NULL,
  `price` decimal(6,2),
  `desc` text,
  `instock` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `category_id` int(10) UNSIGNED NOT NULL,
  `date_add` int(10) UNSIGNED NOT NULL,
  `date_update` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
```
### Table ORDER
```
CREATE TABLE `order` (
  `id` int(10) UNSIGNED  NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `qty` int(4) UNSIGNED NOT NULL DEFAULT 1,
  `date_add` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
```
### Table USER
```
CREATE TABLE `user` (
  `id` int(10) UNSIGNED  NOT NULL AUTO_INCREMENT,
  `email` varchar(120) NOT NULL,
  `pass` varchar(120) NOT NULL,
  `date_add` int(10) UNSIGNED NOT NULL,
  `date_update` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
```