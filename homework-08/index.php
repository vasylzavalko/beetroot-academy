<?php

	$users = [
		"5"	=>	[ "name" => "Ivan", "email" => "ivan@test.com", "lang" => "ua" ],
		"3" =>	[ "name" => "Penro", "email" => "penro@test.com", "lang" => "ru" ],
		"8" =>	[ "name" => "Oksana", "email" => "oksana@test.com", "lang" => "en"],
		"11" =>	[ "name" => "Vasyl", "email" => "vasyl@test.com", "lang" => "de"],
		"1" =>	[ "name" => "Ira", "email" => "ira@test.com", "lang" => "ru"],
		"13" =>	[ "name" => "Lena", "email" => "lena@test.com", "lang" => "en"],
		"18" =>	[ "name" => "Penro", "email" => "penro2@test.com", "lang" => "ua"],
		"14" =>	[ "name" => "Alex", "email" => "alex@test.com", "lang" => "en"],
		"22" =>	[ "name" => "Oksana", "email" => "oksana2@test.com", "lang" => "ua"],
		"10" =>	[ "name" => "Vasyl", "email" => "vasyl2@test.com", "lang" => "fr"],
		"20" =>	[ "name" => "Penro", "email" => "penro3@test.com", "lang" => "ru"],
	];
	
	$messageHello = [
		"ua" => "Привіт",
		"ru" => "Привет",
		"en" => "Hello",
		"de" => "Hallo",
		"fr" => "Bonjour"
	];
	
	$firsUser = reset($users);
	$lastUser = end($users);
	
	if( $firsUser['lang'] == $lastUser['lang'] ){
		$hello = $messageHello[$firsUser['lang']].", ".$firsUser['name'];
		$hello .= " та ".$lastUser['name'];
	}else{
		$hello = $messageHello[$firsUser['lang']].", ".$firsUser['name'];
		$hello .= "<br>".$messageHello[$lastUser['lang']].", ".$lastUser['name'];
	}
	
	echo $hello;
	
?>