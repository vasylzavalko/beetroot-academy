# Homework #19


### Додати дані до таблиць
```
INSERT INTO `user` ( email, pass, date_add, date_update )
VALUES
	( 'test1@vv.loc', '12345', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( 'test2@vv.loc', '12345', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( 'test3@vv.loc', '12345', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( 'test4@vv.loc', '12345', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() );


INSERT INTO `category` ( title )
VALUES
    ( 'Category Name 1' ),
    ( 'Category Name 2' ),
    ( 'Category Name 3' ),
    ( 'Category Name 4' ),
    ( 'Category Name 5' );


INSERT INTO `product` ( sku, title, price, description, instock, category_id, date_add, date_update )
VALUES
	( '101', 'Product title 1', 123.15, 'Description Product 1', 10, 2, UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( '102', 'Product title 2', 238.66, 'Description Product 2', 22, 4, UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( '103', 'Product title 3', 543, 'Description Product 3', 16, 5, UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( '104', 'Product title 4', 43.50, 'Description Product 4', 20, 4, UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( '105', 'Product title 5', 78, 'Description Product 5', 54, 2, UNIX_TIMESTAMP(), UNIX_TIMESTAMP() ),
	( '106', 'Product title 6', 155, 'Description Product 6', 8, 1, UNIX_TIMESTAMP(), UNIX_TIMESTAMP() );


INSERT INTO `order` ( product_id, user_id, qty, date_add )
VALUES
	( 1, 2, 1, UNIX_TIMESTAMP() ),
	( 3, 2, 2, UNIX_TIMESTAMP() ),
	( 5, 2, 1, UNIX_TIMESTAMP() ),
	( 2, 3, 4, UNIX_TIMESTAMP() ),
	( 3, 4, 2, UNIX_TIMESTAMP() );
    
```

### Виведіть загальну кількість всіх товарів
```
SELECT
	SUM( `instock` ) AS all_instock 
FROM
	`product`
```

### Виведіть товар із максимальною кількістю одиниць
```
SELECT
	* 
FROM
	`product` 
ORDER BY
	`instock` DESC 
	LIMIT 1
```

### Поновіть товар із мінімальною кількістю одиниць, збільшивши кількість в 2 рази
```
UPDATE `product` 
SET `instock` = `instock` * 2 
WHERE
	`id` = ( SELECT * FROM ( SELECT `id` FROM `product` ORDER BY `instock` ASC LIMIT 1 ) AS p1 );


# UPDATE `product` 
# SET `instock` = ( SELECT * FROM ( SELECT `instock` FROM `product` ORDER BY `instock` ASC LIMIT 1 ) AS p1 ) * 2 
# WHERE
# 	`id` = ( SELECT * FROM ( SELECT `id` FROM `product` ORDER BY `instock` ASC LIMIT 1 ) AS p1 );
```

### Виведіть дані про всіх користувачів з товаром в кошику і їх товар, який доданий в кошик
```
SELECT
	o.id AS order_id,
	u.id AS user_id,
	u.email,
	p.id AS product_id,
	p.title,
	o.qty 
FROM
	`order` AS o
	INNER JOIN `user` AS u ON u.id = o.user_id
	INNER JOIN `product` AS p ON p.id = o.product_id;
```

### Відсортуйте категорії в порядку зменшення кількості одиниць унікального товару в ній
```
SELECT
	category.title AS category,
	count(*) AS cnt 
FROM
	`product`
	INNER JOIN `category` ON product.category_id = category.id 
GROUP BY
	category.id 
ORDER BY
	cnt DESC
```
