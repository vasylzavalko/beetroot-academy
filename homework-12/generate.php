<?php

$users[1] = [ 'name' => 'petro', 'login' => 'petro', 'pass' => '123456789', 'email' => 'petro@gmail.com', 'lang' => 'ua' ];
$users[2] = [ 'name' => 'oksana', 'login' => 'oksana', 'pass' => '123456', 'email' => 'oksana@gmail.com', 'lang' => 'ru' ];
$users[3] = [ 'name' => 'vasyl', 'login' => 'vasyl', 'pass' => '1234', 'email' => 'vasyl@gmail.com', 'lang' => 'ua' ];
$users[4] = [ 'name' => 'pavlo', 'login' => 'pavlo', 'pass' => '12345', 'email' => 'pavlo@gmail.com', 'lang' => 'en' ];
$users[5] = [ 'name' => 'ira', 'login' => 'ira', 'pass' => '1234567890', 'email' => 'ira@gmail.com', 'lang' => 'fr' ];

// Файли з даними
$foder = __DIR__.DIRECTORY_SEPARATOR."users";
$fileTxt = $foder.DIRECTORY_SEPARATOR.'users.txt';
$fileJson = $foder.DIRECTORY_SEPARATOR.'json.txt';
if (!file_exists($foder)) {
    mkdir($foder, 0777, true);
}

// Генеруємо дату
$dataText = "";
foreach($users as $value){
    $dataText .= $value['name']." ".$value['login']." ".$value['pass']." ".$value['email']." ".$value['lang']."\n";
}
$dataJson = json_encode($users);

// Записуємо в файли
file_put_contents($fileTxt, $dataText);
file_put_contents($fileJson, $dataJson);