<?php

$foder = __DIR__.DIRECTORY_SEPARATOR."users";
$fileTxt = $foder.DIRECTORY_SEPARATOR.'users.txt';
$users = [];

if (file_exists($fileTxt)) {
    $lines = file($fileTxt);
    foreach($lines as $value){
        $valueArray = explode(" ",$value);
        if( count($valueArray)==5 ){
            $users[] = [
                'name' => trim($valueArray[0]),
                'login' => trim($valueArray[1]),
                'pass' => trim($valueArray[2]),
                'email' => trim($valueArray[3]),
                'lang' => trim($valueArray[4]),
            ];
        }
    }
}