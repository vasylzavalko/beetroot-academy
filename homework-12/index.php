<?php

require_once('read_user.php');
require_once('filter_user.php');

// Початковий масив користувачів $users
// Масив користувачів із паролем < 8 символів $filteruser

$str = "";
if (count($filteruser)>0) {
    foreach ($filteruser as $value) {
        $str .= "<p>";
        $str .= "<b>Name: ".$value['name']."</b><br>";
        $str .= "Login: ".$value['login']."<br>";
        $str .= "Email: ".$value['email'];
        $str .= "</p>";
    }
} else {
    $str = "User not found";
}

echo $str;

