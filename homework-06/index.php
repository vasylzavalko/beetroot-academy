<?php
	
	$userData = [
		"name"		=> "Vasyl",
		"phone"		=> "+3(099) 000-00-00",
		"email"		=> "vasylzavalko@gmail.com",
		"pass"		=> "12345",
		"comment"	=> "Коментар"
	];
	
	$userLang = [
		[
			"code" => "ua",
			"name" => "Ukrainian"
		],
		[
			"code" => "ru",
			"name" => "Russian"
		],
		[
			"code" => "en",
			"name" => "English"
		]
	];
	
?>

<!DOCTYPE html>
<html lang="uk" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Форма</title>
	</head>
	<body>
		<h1>Форма</h1>
		<form action="index.php" method="POST">
			<div>
				<input type="text" name="user_name" value="<?php echo $userData['name']; ?>">
			</div>
			<div>
				<input type="text" name="user_phone" value="<?php echo $userData['phone']; ?>">
			</div>
			<div>
				<input type="text" name="user_email" value="<?php echo $userData['email']; ?>">
			</div>
			<div>
				<textarea rows="3" name="user_comment"><?php echo $userData['comment']; ?></textarea>
			</div>
			<div>
				<input type="password" name="user_pass" value="<?php echo $userData['pass']; ?>">
			</div>
			<div>
				<select name="user_lang">
					<option value="<?php echo $userLang[0]['code']; ?>"><?php echo $userLang[0]['name']; ?></option>
					<option value="<?php echo $userLang[1]['code']; ?>"><?php echo $userLang[1]['name']; ?></option>
					<option value="<?php echo $userLang[2]['code']; ?>"><?php echo $userLang[2]['name']; ?></option>
				</select>
			</div>
			<div>
				<button type="submit" name="submit">Надіcлати</button>
			</div>
		</form>
	</body>
</html>