<?php

require_once('config.php');
require_once('models.php');

if (isset($_POST['submit']) && $_POST['submit']=='submit-order' ) {
    saveCart(); // Записуємо обрані товари в сесію
}
if (isset($_POST['submit']) && $_POST['submit']=='submit-clear' ) {
    clearCart(); // Видаляємо сесію
}

$cart = getCart(); // Якщо є сесія із товарами, то записуємо у змінну $cart
$products = readProducts("products.json"); // Генеруємо масив товарів на основі JSON файлу
$products = checkProducts($products, $cart); // Додаємо в масив значення товар чи обраний чи ні
$order = order($cart, $products); // Генеруємо масив обраних товарів

require_once( __DIR__.DIRECTORY_SEPARATOR.TEMPLATE.DIRECTORY_SEPARATOR.'header.php');
require_once( __DIR__.DIRECTORY_SEPARATOR.TEMPLATE.DIRECTORY_SEPARATOR.'main.php');
require_once( __DIR__.DIRECTORY_SEPARATOR.TEMPLATE.DIRECTORY_SEPARATOR.'footer.php');

