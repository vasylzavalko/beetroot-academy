<?php

require_once('config.php');

$catalog['268103536'] = [
    'sku' => '268103536',
    'title' => 'Мобільний телефон Xiaomi Redmi Note 9 Pro 6/64GB Tropical Green',
    'desc' => 'Екран (6.67", IPS, 2400x1080) / Qualcomm Snapdragon 720G (2 x 2.3 ГГц + 6 x 1.8 ГГц) / основна квадрокамера: 64 Мп + 8 Мп + 5 Мп + 2 Мп, фронтальна 16 Мп / RAM 6 ГБ / 64 ГБ вбудованої пам\'яті + microSD / 3G / LTE / GPS / підтримка 2 SIM-карток (Nano-SIM) / Android 10 / 5020 мА·год',
    'price' => '6499',
    'instock' => '10',
];
$catalog['268107536'] = [
    'sku' => '268107536',
    'title' => 'Мобільний телефон Samsung Galaxy A02s 3/32 GB Black',
    'desc' => 'Екран (6.5", PLS, 1600x720) / Qualcomm Snapdragon 450 (1.8 ГГц) / потрійна основна камера: 13 Мп + 2 Мп + 2 Мп, фронтальна 5 Мп / RAM 3 ГБ / 32 ГБ вбудованої пам\'яті + microSD (до 1 ТБ) / 3G / LTE / GPS / підтримка 2 SIM-карток (Nano-SIM) / Android 10 / 5000 мА·год',
    'price' => '3499',
    'instock' => '12',
];
$catalog['294048833'] = [
    'sku' => '294048833',
    'title' => 'Мобільний телефон Samsung Galaxy M12 4/64 GB Black',
    'desc' => 'Екран (6.5", PLS, 1600x720) / Samsung Exynos 850 (2.0 ГГц) / основна квадрокамера: 48 Мп + 5 Мп + 2 Мп + 2 Мп, фронтальна камера: 8 Мп / RAM 4 ГБ / 64 ГБ вбудованої пам\'яті + microSD (до 1 ТБ) / 3G / LTE / GPS / підтримка 2 SIM-карток (Nano-SIM) / Android 11 / 5000 мА·год',
    'price' => '4599',
    'instock' => '3',
];
$catalog['155746186'] = [
    'sku' => '155746186',
    'title' => 'Мобільний телефон ZTE Blade 20 Smart 4/128GB Gradient Green',
    'desc' => 'Екран (6.49", IPS, 1560x720) / MediaTek Helio P60 (8 x 2.0 ГГц) / потрійна основна камера: 16 Мп + 8 Мп + 2 Мп, фронтальна камера: 13 Мп / RAM 4 ГБ / 128 ГБ вбудованої пам\'яті + microSD (до 512 ГБ) / 3G / LTE / GPS / ГЛОНАСС / підтримка 2 SIM-карток (Nano-SIM) / Android 9.0 (Pie) / 5000 мА·год',
    'price' => '3899',
    'instock' => '5',
];
$catalog['199325431'] = [
    'sku' => '199325431',
    'title' => 'Мобільний телефон Samsung Galaxy A31 4/64GB Prism Crush Blue',
    'desc' => 'Екран (6.4", Super AMOLED, 2400x1080) / MediaTek MT6768 (2 x 2.0 ГГц + 6 x 1.7 ГГц) / основна квадрокамера: 48 Мп + 8 Мп + 5 Мп + 5 Мп, фронтальна камера: 20 Мп / RAM 4 ГБ / 64 ГБ вбудованої пам\'яті + microSD (до 512 ГБ) / 3G / LTE / GPS / ГЛОНАСС / BDS / підтримка 2 SIM-карток (Nano-SIM) / Android 10.0 (Q) / 5000 мА·год',
    'price' => '5999',
    'instock' => '',
];
$catalog['282416913'] = [
    'sku' => '282416913',
    'title' => 'Мобільний телефон Xiaomi Redmi 9T 4/128 Twilight Blue',
    'desc' => 'Екран (6.53", 2340x1080) / Qualcomm Snapdragon 662 (4 х 2.0 ГГц + 4 х 1.8 ГГц) / основна квадрокамера: 48 Мп + 8 Мп + 2 Мп + 2 Мп, фронтальна 8 Мп / RAM 4 ГБ / 128 ГБ вбудованої пам\'яті + microSD (до 512 ГБ) / 3G / LTE / GPS / A-GPS / ГЛОНАСС / BDS / підтримка 2 SIM-карток (Nano-SIM) / Android 10 (Q) / 6000 мА·год',
    'price' => '5499',
    'instock' => '9',
];
$catalog['260862296'] = [
    'sku' => '260862296',
    'title' => 'Мобільний телефон Apple iPhone 11 64 GB Black Slim Box',
    'desc' => 'Екран (6.1", IPS (Liquid Retina HD), 1792x828) / Apple A13 Bionic / основна подвійна камера: 12 Мп + 12 Мп, фронтальна камера: 12 Мп / RAM 4 ГБ / 64 ГБ вбудованої пам\'яті / 3G / LTE / GPS / ГЛОНАСС / Nano-SIM / iOS 13 / 3046 мА·год',
    'price' => '21499',
    'instock' => '6',
];
$catalog['217749301'] = [
    'sku' => '217749301',
    'title' => 'Мобільний телефон Samsung Galaxy A11 2/32GB Red',
    'desc' => 'Екран (6.4", PLS, 1560x720) / Qualcomm Snapdragon 450 (8 x 1.8 ГГц) / потрійна основна камера: 13 Мп + 5 Мп + 2 Мп, фронтальна камера: 8 Мп / RAM 2 ГБ / 32 ГБ вбудованої пам\'яті + microSD (до 512 ГБ) / 3G / LTE / GPS / ГЛОНАСС / BDS / підтримка 2 SIM-карток (Nano-SIM) / Android 10.0 (Q) / 4000 мА·год',
    'price' => '3099',
    'instock' => '12',
];
$catalog['168814171'] = [
    'sku' => '168814171',
    'title' => 'Мобільний телефон Samsung Galaxy A51 6/128GB Blue',
    'desc' => 'Екран (6.5", Super AMOLED, 2400x1080) / Samsung Exynos 9611 (2.3 ГГц + 1.7 ГГц) / основна квадрокамера: 48 Мп + 12 Мп + 5 Мп + 5 Мп, фронтальна 32 Мп / RAM 6 ГБ / 128 ГБ вбудованої пам\'яті + microSD (до 512 ГБ) / 3G / LTE / GPS / підтримка 2 SIM-карток (Nano-SIM) / Android 10.0 (Q) / 4000 мА·год',
    'price' => '7599',
    'instock' => '10',
];
$catalog['245565619'] = [
    'sku' => '245565619',
    'title' => 'Мобільний телефон Motorola E6s 4/64GB Meteor Grey',
    'desc' => 'Екран (6.1", IPS, 1560x720) / MediaTek Helio P22 (2.0 ГГц) / подвійна основна камера: 13 Мп + 2 Мп, фронтальна камера: 5 Мп / RAM 4 ГБ / 64 ГБ вбудованої пам\'яті + microSD (до 256 ГБ) / 3G / LTE / GPS / підтримка 2 SIM-карток (Nano-SIM) / Android 9.0 (Pie) / 3000 мА·год',
    'price' => '2699',
    'instock' => '9',
];

// Файли з даними
$foder = __DIR__.DIRECTORY_SEPARATOR.CATALOG;
$fileJson = $foder.DIRECTORY_SEPARATOR.'products.json';
if (!file_exists($foder)) {
    mkdir($foder, 0777, true);
}

$dataJson = json_encode($catalog, JSON_UNESCAPED_UNICODE);      
file_put_contents($fileJson, $dataJson);