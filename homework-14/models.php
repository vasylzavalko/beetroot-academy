<?php

function readProducts($fileName)
{
    $products = [];
    $foder = __DIR__.DIRECTORY_SEPARATOR."catalog";
    $fileJson = $foder.DIRECTORY_SEPARATOR.$fileName;    
    if (file_exists($fileJson)) {
        $products = file_get_contents($fileJson);
        $products = json_decode($products,TRUE);
    }
    return $products;
}

function checkProducts($products,$cart)
{
    $cartIds = array_keys($cart);
    foreach ($products as &$value) {
        if (in_array($value['sku'], $cartIds)) {
            $value['checked'] = "checked";
            $value['class'] = "product-checked";
        } else {
            $value['checked'] = "";
            $value['class'] = "";
        }
    }
    return $products;
}

function saveCart()
{
    $cart = [];
    $productIds = trim($_POST['product-items']);
    if (empty($productIds)) {
        clearCart();
    } else {
        $productIds = explode("|", $productIds);
        if (count($productIds)>0) {
            foreach ($productIds as $value) {
                $cart[$value] = 1;
            }
            $_SESSION['cart'] = $cart;
            header("Location: ". $_SERVER["REQUEST_URI"]);
        }
    }
}

function clearCart()
{
    session_unset();
    header("Location: ". $_SERVER["REQUEST_URI"]);
}

function getCart()
{
    $cart = [];
    if (isset($_SESSION['cart'])) {
        $cart = $_SESSION['cart'];
    }
    return $cart;
}

function order($cart,$products)
{
    $order = [];
    $total = 0;
    if (count($cart)>0) {
        foreach ($cart as $sku => $qty) {
            $order['products'][$sku]['sku'] = $sku;
            $order['products'][$sku]['title'] = $products[$sku]['title'];
            $order['products'][$sku]['price'] = $products[$sku]['price'];
            $order['products'][$sku]['qty'] = $qty;
            $total = $total + ( $products[$sku]['price'] * $qty );
        }
        $order['total'] = $total;
    }
    return $order;
}