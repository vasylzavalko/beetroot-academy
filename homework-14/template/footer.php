<footer class="uk-text-center uk-padding uk-text-bold">Homework #14</footer></body></html>
<script>
$(document).ready(function(){
    function checkProduct(){
        var products = [];
        $.each($("input[name='product-item']:checked"), function(){            
            products.push($(this).val());
        });		
        $("input[name='product-items']").val(products.join("|"));        
    }
    checkProduct();
    $(".product").click(function(){
        var productId = $(this).data('id');
        console.log(productId);
        var productCheck = $(".product-item-"+productId);
        $(this).toggleClass("product-checked");
        if (productCheck.prop("checked")==true) {
            productCheck.prop("checked", false);
        } else {
            productCheck.prop("checked", true);
        }
        checkProduct();
    });
})
</script>