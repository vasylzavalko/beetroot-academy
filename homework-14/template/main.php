<main data-uk-height-viewport="expand: true">
    <div class="uk-container">
        <div data-uk-grid data-uk-height-match="target: .height">
            <?php if (count($order)>0) { ?>
            <div class="uk-width-1-1">
                <div class="box">
                    <div class="box-label">Обрані товари</div>
                    <table class="uk-table uk-table-small uk-table-divider uk-table-justify">
                        <tr>
                            <th class="uk-table-shrink uk-text-nowrap">SKU</th>
                            <th class="uk-text-bold">Товар</th>
                            <th class="uk-table-shrink uk-text-nowrap">К-сть</th>
                            <th class="uk-table-shrink uk-text-nowrap">Вартість</th>
                        </tr>
                        <?php foreach ($order['products'] as $value) { ?>
                        <tr>
                            <td class="uk-table-shrink uk-text-nowrap"><?php echo $value['sku'] ?></td>
                            <td class="uk-text-bold"><?php echo $value['title'] ?></td>
                            <td class="uk-table-shrink uk-text-nowrap"><?php echo $value['qty'] ?> шт.</td>
                            <td class="uk-table-shrink uk-text-nowrap"><?php echo $value['price'] ?> &#8372;</td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="uk-table-shrink uk-text-nowrap uk-text-bold uk-text-center"><?php echo $order['total'] ?> &#8372;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <?php } ?>
            <?php foreach ($products as $value) { ?>
            <div class="uk-width-1-4">
                <div class="uk-box-shadow-small uk-box-shadow-hover-xlarge uk-padding-small height product <?php echo $value['class'] ?>" data-id="<?php echo $value['sku'] ?>">
                    <div data-uk-grid class="uk-grid-small">
                        <div class="uk-width-1-1 uk-text-bold">
                            <?php echo $value['title'] ?>
                        </div>
                        <div class="uk-width-1-1 uk-text-small">
                            <?php echo $value['desc'] ?>
                        </div>
                        <div class="uk-width-1-1 uk-text-danger uk-text-bold">
                            <?php echo $value['price'] ?> &#8372;
                            <input <?php echo $value['checked'] ?> type="checkbox" name="product-item" value="<?php echo $value['sku'] ?>" class="product-item-<?php echo $value['sku'] ?>" hidden>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="uk-width-1-1"><hr></div>
            <div class="uk-width-expand">
                <form method="POST" action="/homework-14/" >
                    <button class="uk-button uk-button-default" type="submit" name="submit" value="submit-clear">Видалити товари із кошика</button>
                </form>
            </div>
            <div class="uk-width-auto">
                <form method="POST" action="/homework-14/" >
                    <input name="product-items" value="" hidden>
                    <button class="uk-button uk-button-primary" type="submit" name="submit" value="submit-order">Замовити</button>
                </form>
            </div>
        </div>
    </div>
</main>