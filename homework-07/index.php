<?php

	$users = [
		"5"	=> ["name" => "Ivan", "email" => "ivan@test.com"],
		"3" => ["name" => "Penro", "email" => "penro@test.com"],
		"8" => ["name" => "Oksana", "email" => "oksana@test.com"],
		"11" => ["name" => "Vasyl", "email" => "vasyl@test.com"],
		"1" => ["name" => "Ira", "email" => "ira@test.com"],
		"13" => ["name" => "Lena", "email" => "lena@test.com"],
		"9" => ["name" => "Alex", "email" => "alex@test.com"]
	];
		
	echo "Загальна кількість користувачів: ".count($users)."<br>";

	// сортуємо за спаданням ID
	krsort($users);

	// масив із значеннями ID користувачів
	$userId = array_keys($users);

	// Варіант 1
	$userMaxId = max($userId); // максимальний ID
	$userMinId = min($userId); // мінімальний ID
	$userPrevMaxId = next($userId); // перед максимальним ID
	$userNextMinId = end($userId);
	$userNextMinId = prev($userId); // наступний за мінімальним ID

	// Варіант 2
	$userMaxId = $userId[0]; // максимальний ID
	$userMinId = $userId[count($userId)-1]; // мінімальний ID
	$userPrevMaxId = $userId[1]; // перед максимальним ID
	$userNextMinId = $userId[count($userId)-2]; // наступний за мінімальним ID

	echo "Користувач із максимальним ID: ".$users[$userMaxId]['name']." (".$userMaxId.")<br>";
	echo "Користувач із мінімальним ID: ".$users[$userMinId]['name']." (".$userMinId.")<br>";
	echo "Користувач перед максимальним ID: ".$users[$userPrevMaxId]['name']." (".$userPrevMaxId.")<br>";
	echo "Користувач наступний за мінімальним ID: ".$users[$userNextMinId]['name']." (".$userNextMinId.")<br>";

	// видалення користувача із мінімальним ID
	unset($users[$userMinId]);	
	
?>