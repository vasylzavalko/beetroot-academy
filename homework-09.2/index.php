<?php

$textNumOne = ['ноль','один','два','три','чотири','п\'ять','шість','сім','вісім','дев\'ять'];
$textNumTen = ['десять','одинадцять','дванадцять','тринадцять','чотирнадцять','п\'ятнадцять','шістнадцять','сімнадцять','вісімнадцать','дев\'ятнадцять'];
$textNumTens = ['','','двадцять','тридцять','сорок','п\'ятдесят','шістдесят','сімдесят','вісімдесят','дев\'яносто'];
$textNumHundred = ['','сто','двісті','триста','чотириста','п\'ятсот','шістсот','сімсот','вісімсот','дев\'ятсот'];
$num = rand(0,999);

for( $i=0; $i<1000; $i++){

	$num = $i;
	$numBreak = 0;
	$numStr = $num."";
	$str = "";
	$numStrlen = strlen($numStr);

	if( strlen($numStr) == 3 ){
		$str .= $textNumHundred[$numStr[0]];
		$num = $num - ($numStr[0]*100);
		$numStr = $num."";
		if($num==0){
			$numBreak = 1;
		}
	}
	
	if( strlen($numStr) == 2 ){
		if($num<20 AND $num>9){
			$str .= " ".$textNumTen[$numStr[1]];
			$numBreak = 1;
		}else{
			$str .= " ".$textNumTens[$numStr[0]];
		}
		$num = $num - ($numStr[0]*10);
		$numStr = $num."";
		if($num==0){
			$numBreak = 1;
		}
	}

	if( strlen($numStr) == 1 AND $numBreak == 0 ){
		if($numStrlen>1 AND $num!=0 ){
			$str .= " ".$textNumOne[$numStr[0]];
		}else{
			$str = $textNumOne[$numStr[0]];
		}
	}

	echo $i." - ".$str."<br>";
	
}	
	
?>