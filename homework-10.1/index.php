<?php

	// Завдання 1
	$date = "31-12-2020";
	$dateFormat = explode("-", $date);
	$dateFormat = array_reverse($dateFormat);
	$dateFormat = implode(".", $dateFormat);
	echo $date." - ".$dateFormat;
	
	// Завдання 2
	echo "<br><br>";
	$str = "london is the capital of great britain";
	$strFormat = ucwords($str);
	echo $str." - ".$strFormat;
	
	// Завдання 3
	echo "<br><br>";
	$password = "qwertyuiop";
	if( mb_strlen($password)>7 AND mb_strlen($password)<12 ){
		$str = "Ваш пароль підходить.";
	}else{
		$str = "Введіть новий пароль від 8 до 11 символів";
	}
	echo $str;
	
	// Завдання 4
	echo "<br><br>";
	$str = "1a2b3c4b5d6e7f8g9h0";
	$strFormat = preg_replace('/\d/', '', $str);
	echo $str." - ".$strFormat;
	
	
?>