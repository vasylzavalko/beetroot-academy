<?php

session_start();
define("TEMPLATE", "template");
define("DIR_TEMPLATE", __DIR__.DIRECTORY_SEPARATOR.TEMPLATE.DIRECTORY_SEPARATOR);
define("HOST", "localhost");
define("DBNAME", "test");
define("DBUSER", "user");
define("DBPASS", "12345");

try {
    $db = new PDO('mysql:host='.HOST.';dbname='.DBNAME, DBUSER, DBPASS);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage();
    die();
}

