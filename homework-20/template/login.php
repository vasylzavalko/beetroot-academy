<main data-uk-height-viewport="expand: true">
    <div class="uk-container">
        <div data-uk-grid>
            <div class="uk-width-1-2">
                <form method="POST" action="/homework-20/" data-uk-grid class="uk-grid-small">
                    <div class="uk-width-1-1">
                        <h2>Авторизація</h2>
                    </div>
                    <?php if($message['type']=='login') { ?>
                    <div class="uk-width-1-1 uk-text-bold uk-text-danger">
                        <?php echo $message['message'] ?>
                    </div>
                    <?php } ?>
                    <div class="uk-width-1-1">
                        <label>Email</label>
                        <input class="uk-input uk-width-1-1" name="email" type="text" value="" required>
                    </div>
                    <div class="uk-width-1-1">
                        <label>Пароль</label>
                        <input class="uk-input uk-width-1-1" name="password" type="password" value="" required>
                    </div>
                    <div class="uk-width-1-1">
                        <button class="uk-button uk-button-primary" type="submit" name="submit" value="submit-login">Увійти</button>
                    </div>
                </form>
            </div>
            <div class="uk-width-1-2">
                <form method="POST" action="/homework-20/" data-uk-grid class="uk-grid-small">
                    <div class="uk-width-1-1">
                        <h2>Реєстрація</h2>
                    </div>
                    <?php if($message['type']=='register') { ?>
                    <div class="uk-width-1-1 uk-text-bold uk-text-danger">
                        <?php echo $message['message'] ?>
                    </div>
                    <?php } ?>
                    <div class="uk-width-1-1">
                        <label>Email</label>
                        <input class="uk-input uk-width-1-1" name="email" type="text" value="" required>
                    </div>
                    <div class="uk-width-1-2">
                        <label>Пароль</label>
                        <input class="uk-input uk-width-1-1" name="password" type="password" value="" required>
                    </div>
                    <div class="uk-width-1-2">
                        <label>Повтории пароль</label>
                        <input class="uk-input uk-width-1-1" name="password_confirm" type="password" value="" required>
                    </div>
                    <div class="uk-width-1-1">
                        <button class="uk-button uk-button-primary" type="submit" name="submit" value="submit-register">Зареєструватись</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>