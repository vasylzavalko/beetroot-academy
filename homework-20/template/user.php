<main data-uk-height-viewport="expand: true">
    <div class="uk-container">
        <div data-uk-grid>
            <div class="uk-width-1-1"><h1>Сторінка користувач</h1></div>
            <div class="uk-width-expand">
                <p><b>Email:</b> <?php echo $user['email'] ?></p>
            </div>
            <div class="uk-width-auto">
                <form method="POST" action="/homework-20/">
                    <button class="uk-button uk-button-primary" type="submit" name="submit" value="submit-logout">Вийти</button>
                </form>
            </div>
        </div>
    </div>
</main>