<?php

require_once('config.php');
require_once('models.php');

if (isset($_POST['submit']) && $_POST['submit']=='submit-login' ) {
    login($db); // Авторизація
}
if (isset($_POST['submit']) && $_POST['submit']=='submit-register' ) {
    register($db); // Реєстрація
}
if (isset($_POST['submit']) && $_POST['submit']=='submit-logout' ) {
    logout($db); // Вийти
}

$user = getUser($db);
$message = getMessage();

require_once(DIR_TEMPLATE.'header.php');
if ($user['login']==false) {
    require_once(DIR_TEMPLATE.'login.php');
} else {
    require_once(DIR_TEMPLATE.'user.php');
}
require_once(DIR_TEMPLATE.'footer.php');