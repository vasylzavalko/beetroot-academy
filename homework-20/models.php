<?php

function getUser($db)
{
    $user = [
        'login' => false
    ];
    if (isset($_SESSION['user'])) {
        $sessionUser = $_SESSION['user'];
        if ($sessionUser['login']==true && isset($sessionUser['id']) && is_numeric($sessionUser['id'])) {
            $stmt = $db->query("SELECT * FROM `user` WHERE `id` = ".$sessionUser['id']);
            while ($row = $stmt->fetch())
            {
                $user = [
                    'id' => $row['id'],
                    'email' => $row['email'],
                    'login' => true
                ];
            }
        }      
    }
    return $user;
}

function getMessage()
{
    $message = [
        'type' => 'none'
    ];
    if (isset($_SESSION['flash'])) {
        $message = [
            'type' => $_SESSION['flash']['type'],
            'message' => $_SESSION['flash']['message']
        ];
    }
    if(!isset($_POST['submit']) && !empty($_SESSION['flash']))
    {
        unset($_SESSION['flash']);
    }
    return $message;
}

function login($db)
{
    $loginId = 0;
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $data = $db->prepare("SELECT `id` FROM `user` WHERE `email` = ? AND `pass` = ?");
    $data->bindParam(1, $email);
    $data->bindParam(2, $password);
    $data->execute();
    if ($data->rowCount() > 0) {
        $loginId = $data->fetch(PDO::FETCH_COLUMN);
        if($loginId>0){
            $user = [
                'id' => $loginId,
                'login' => true
            ];
            $_SESSION['user'] = $user;
        }
    } else {
        $message = [
            'type' => 'login',
            'message' => 'Користувач із введеними даними не знайдений.'
        ];
        $_SESSION['flash'] = $message;
    }
    header("Location: ". $_SERVER["REQUEST_URI"]);
}

function logout()
{
    session_unset();
    header("Location: ". $_SERVER["REQUEST_URI"]);
}

function register($db)
{
    $userId = 0;
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $passwordConfirm = trim($_POST['password_confirm']);
    if($password==$passwordConfirm){
        $data = $db->prepare("SELECT `id` FROM `user` WHERE `email` = ?");
        $data->bindParam(1, $email);
        $data->execute();
        if ($data->rowCount() > 0) {
            $userId = $data->fetch(PDO::FETCH_COLUMN);
        }
        if ($userId==0) {
            $time = time();
            $data = $db->prepare("INSERT INTO `user` SET `email` = ?, `pass` = ?, `date_add` = ".$time.", `date_update` = ".$time);
            $data->bindParam(1, $email);
            $data->bindParam(2, $password);
            $data->execute();
            $message = [
                'type' => 'register',
                'message' => 'Ви успішно зареєструвались! Можете увійти в свій аккаунт.'
            ];
            $_SESSION['flash'] = $message;
        } else {
            $message = [
                'type' => 'register',
                'message' => 'Користувач із введеними Email вже зареєстрований.'
            ];
            $_SESSION['flash'] = $message;
        }
    } else {
        $message = [
            'type' => 'register',
            'message' => 'Введені паролі не співпадають'
        ];
        $_SESSION['flash'] = $message;
    }    
    header("Location: ". $_SERVER["REQUEST_URI"]);
}